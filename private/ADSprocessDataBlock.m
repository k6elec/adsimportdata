function res = ADSprocessDataBlock(dataBlock)
% ADSprocessDataBlock gets the simulation data from a given dataBlock

% a dataBlock contains the following fields:
%
%   name        name of the simulation block in ADS.
%   type        simulation type
%   sweeps      name of the swept variables
%   independent name of the independent variable (most of the time 'freq')
%   dependents  cell array which contains the names of the dependent variables
%   data        structure which contains the simulation data
%
dependents=dataBlock.dependents;
if ~isempty(dataBlock.data)
    data = dataBlock.data;
else
    error('no data was provided by ADS');
end

% get the data out of
for ii=1:length(dependents)
    currentdata=zeros(length(data),length(data(1).independent));
    for jj=1:length(data)
        if ~isempty(data(1,jj).dependents)
            currentdata(jj,:)=data(1,jj).dependents(ii,:);
        else
            error('the dependents are empty, ADS gave no data')
        end
    end
    res.(genvarnamecurrent(dependents{1,ii}))=currentdata;
end
% check whether there is a swept variable present
if ~isempty(dataBlock.sweeps)
    
    % there is a parameter sweep present take it into account.
    % if there are N sweep variables, I want an N+1 dimensional matrix with
    % the results. so a reshape has to be performed.
    % problem is we don't know how many sweeps there are of each variable,
    % so we have to find out first
    
    % initiate the sweepvals cell array
    sweepvals = cell(length(dataBlock.sweeps),1);
    for ii=1:length(sweepvals)
        sweepvals{ii}=zeros(length(data),1);
    end
    % fill the sweepvals cell array with all the sweep values found in the datablocks
    for ii=1:length(data)
        for jj=1:length(data(ii).sweep)
            sweepvals{jj}(ii)=data(ii).sweep{jj};
        end
    end
    % find out how many unique values there are in each cell of the matrix
    reshapepat=zeros(1,length(sweepvals));
    for ii=1:length(sweepvals)
        reshapepat(ii)=length(unique(sweepvals{ii}));
    end
    % add the independent variable as the last dimension of the reshape
    reshapepat(end+1)=length(data(1).independent);
    % reshape all of the datablocks to the right dimension
    varnames=fieldnames(res);
    for ii=1:length(varnames)
        res.(varnames{ii})=reshape(res.(varnames{ii}),reshapepat);
    end
    for ii=1:length(sweepvals)
        % ADS flips the sweepvals, so we have to adress them from the back this should fix the bug Matthias found.
        res.(['sweepvar_' genvarname(dataBlock.sweeps{length(sweepvals)+1-ii})])=unique(sweepvals{ii});
    end
end

end

%% function to generate the variable name to adams liking
function res=genvarnamecurrent(str)
% if the fieldname ends in .i then we are talking about a current.
% change the generated name then to I_
temp=genvarname(str);
if length(temp)>=5
    if strcmp(temp(end-4:end),'0x2Ei')
        % if the string already begins with I_, then
        if strcmp(temp(1:2),'I_')
            res=temp(1:end-5);
        else
            res=['I_' temp(1:end-5)];
        end
    else
        res=genvarname(str);
    end
else
    res=genvarname(str);
end
end

